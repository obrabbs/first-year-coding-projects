import SearchFunctions
import SortFunctions

#Making initial list
List = []
NumElements = int(input("Number of elements in unordered list: "))
for i in range(0, NumElements):
    element = int(input("Number: "))
    List.append(element)

#Linear Search
Value = int(input("Target value for linear search: "))
Position = SearchFunctions.LinearSearch(List, Value)
if(Position == -1):
    print(f"{Value} is not in your list")
else:
    print(f"{Value} is in your list at index {Position}")

#Binary Search
Value = int(input("Target value for binary search: "))
Position = SearchFunctions.BinarySearch(List, Value, NumElements)
if(Position == -1):
    print(f"{Value} is not in your list")
else:
    print(f"{Value} is in your list at index {Position}")


#Bubble Sort
ol = SortFunctions.BubbleSort(List)
print(f"Original list: {List}")
print(f"New ordered list: {ol}")


#Merge Sort


#Insertion Sort

