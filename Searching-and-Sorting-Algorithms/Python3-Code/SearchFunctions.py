import SortFunctions

def LinearSearch(ul, num):
    for i in range(0, len(ul)-1):
        if(ul[i] == num):
            return i
        elif(ul[i] != num and i == len(ul)):
            return -1

def BinarySearch(ul, Num, Length):
    if(Length < 10):
        ol = SortFunctions.BubbleSort(ul)
    elif(Length < 20):
        ol = SortFunctions.InsertionSort(ul)
    else:
        ol = SortFunctions.MergeSort(ul)
    Low = 0
    High = Length
    Found = False
    while High >= Low:
        mid = int((High + Low) / 2)
        if(ol[mid] == Num):
            return mid
        elif(ol[mid] > Num):
            UpperBound = mid - 1
        else:
            LowerBound = mid + 1
    return -1
        



